export const Environment = {
  production: false, // set TRUE before you build and release a prod version.
  // Set your app configurations here.
  // For the list of config options, please refer to https://ionicframework.com/docs/api/config/Config/
  config: {
    // iconMode: 'md',
    // mode: 'md'
    // iconMode: 'ios',
    // mode: 'ios'
    preloadModules: true,
    scrollPadding: false,
    scrollAssist: true,
    autoFocusAssist: false,
    menuType: 'overlay'
  },
  // Set language to use.
  language: 'en',
  // Firebase Cloud Messaging Server Key.
  // Get your gcmKey on https://console.firebase.google.com, under Overview -> Project Settings -> Cloud Messaging.
  // This is needed to send push notifications.
  gcmKey: 'AAAAem7bVZo:APA91bFfrNXYY0Dy3dAd9wwQrPkP0JA86i3Jb0zx8GJfRpujvPTmqEHjUODifMw38ubEApgevQC247yTiZjUxS8kzCLzff4sSWK6Rfz1RnrbzBUuMa5gm0gO9gI7s8XEPDfF0_SAAE04',
  // Set to your Firebase app, you can find your credentials on Firebase app console -> Add Web App.
  firebase: {
    apiKey: "AIzaSyAtzb4e6XWh-r91_PO-WV9-Iekas4hxPqY",
    authDomain: "taxiap-c5bad.firebaseapp.com",
    databaseURL: "https://taxiap-c5bad.firebaseio.com",
    projectId: "taxiap-c5bad",
    storageBucket: "taxiap-c5bad.appspot.com",
    messagingSenderId: "1049790250065"
  },
  // You can find your googleWebClientId on your Firebase app console -> Authentication -> Sign-in Method -> Google -> Web client ID
  googleWebClientId: '525845878170-4nkapa764tg54c7ub4dqeroj56dumukk.apps.googleusercontent.com',
  // Google Maps JavaScript API Key
  googleMapsKey: 'AIzaSyBm8QolOxKatPeiyviWojKA_RXsMKB74lw',
  // Loading Configuration.
  // Please refer to the official Loading documentation here: https://ionicframework.com/docs/api/components/loading/LoadingController/
  loading: {
    spinner: 'circles'
  },
  // Toast Configuration.
  // Please refer to the official Toast documentation here: https://ionicframework.com/docs/api/components/toast/ToastController/
  toast: {
    position: 'bottom' // Position of Toast, top, middle, or bottom.
  },
  toastDuration: 3000 // Duration (in milliseconds) of how long toast messages should show before they are hidden.
}
