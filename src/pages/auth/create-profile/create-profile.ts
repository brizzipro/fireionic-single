import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ActionSheetController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { AuthProvider, TranslateProvider, FirestoreProvider, LoadingProvider, StorageProvider, NetworkProvider, NotificationProvider } from '../../../providers';
import { Keyboard } from '@ionic-native/keyboard';
import { Camera } from '@ionic-native/camera';
import { User } from '../../../models';
import firebase from 'firebase';

@IonicPage({
  name: 'page-create-profile',
  segment: 'create-profile',
  priority: 'high'
})

@Component({
  selector: 'page-create-profile',
  templateUrl: 'create-profile.html',
})

export class CreateProfilePage {
  private profileForm: FormGroup;
  private photo: string = 'assets/img/profile.png';
  private userId: string;
  private hasError: boolean;
  private uniqueUsername: boolean;
  private nameValidator: ValidatorFn = Validators.compose([
    Validators.required
  ]);
  private usernameValidator: ValidatorFn = Validators.compose([
    Validators.pattern('^[0-z.]{4,20}$'),
    Validators.required
  ]);
  private emailValidator: ValidatorFn = Validators.compose([
    Validators.required,
    Validators.email
  ]);

  constructor(private navCtrl: NavController,
    private navParams: NavParams,
    private menuCtrl: MenuController,
    private actionSheetCtrl: ActionSheetController,
    private formBuilder: FormBuilder,
    private auth: AuthProvider,
    private translate: TranslateProvider,
    private firestore: FirestoreProvider,
    private loading: LoadingProvider,
    private storage: StorageProvider,
    private network: NetworkProvider,
    private notification: NotificationProvider,
    private keyboard: Keyboard,
    private camera: Camera) {
    this.profileForm = formBuilder.group({
      nombre: ['', this.nameValidator],
      apellido1: ['', this.nameValidator],
      username: ['', this.usernameValidator],
      email: ['', this.emailValidator]
    });
  }

  keyDownFunction(event) {
    // User pressed return on keypad, proceed with creating profile.
    if (event.keyCode == 13) {
      this.keyboard.close();
      this.createProfile();
    }
  }

  onInput(username: string) {
    // Check if the username entered on the form is still available.
    this.uniqueUsername = true;
    if (this.profileForm.controls.username.valid && !this.profileForm.controls.username.hasError('required')) {
      this.firestore.getUserByUsername('@' + username.toLowerCase()).then((user: User) => {
        if (user) {
          this.uniqueUsername = false;
        }
      }).catch(() => { });
    }
  }

  ionViewDidLoad() {
    // Disable sideMenu.
    this.menuCtrl.enable(false);
    // Fill up the form with relevant user info based on the authenticated user on Firebase.
    this.auth.getUser().then((user: firebase.User) => {
      this.userId = user.uid;
      if (user.photoURL) {
        this.photo = user.photoURL;
      }
      let nombre = '';
      let apellido1 = '';
      if (user.displayName) {
        nombre = user.displayName.substr(0, user.displayName.indexOf(' '));
        apellido1 = user.displayName.substr(user.displayName.indexOf(' ') + 1, user.displayName.length);
      }
      this.profileForm.setValue({
        nombre: nombre,
        apellido1: apellido1,
        username: '',
        email: user.email
      });
    }).catch(() => { });
  }

  ionViewWillLeave() {
    // Check if userData exists on Firestore. If no userData exists yet, delete the photo uploaded to save Firebase storage space.
    this.firestore.exists('users/' + this.userId).then(exists => {
      if (!exists) {
        this.storage.delete(this.userId, this.photo);
      }
    }).catch(() => { });
  }

  private createProfile(): void {
    // Check if profileForm is valid and username is unique and proceed with creating the profile.
    if (!this.profileForm.valid || !this.uniqueUsername) {
      this.hasError = true;
    } else {
      if (this.uniqueUsername) {
        this.loading.show();
        // Create userData on Firestore.
        this.firestore.get('users/' + this.userId).then(ref => {
          // Formatting the first and last names to capitalized.
          let nombre = this.profileForm.value['nombre'].charAt(0).toUpperCase() + this.profileForm.value['nombre'].slice(1).toLowerCase();
          let apellido1 = this.profileForm.value['apellido1'].charAt(0).toUpperCase() + this.profileForm.value['apellido1'].slice(1).toLowerCase();
          let user = new User(this.userId, this.profileForm.value['email'].toLowerCase(), nombre, apellido1, this.photo, '@' + this.profileForm.value['username'].toLowerCase(), '', true);
          ref.set(user.obj).then(() => {
            this.notification.init();
            this.loading.hide();
            this.navCtrl.push('page-loader');
          }).catch(() => { });
        }).catch(() => { });
      }
    }
  }

  private setPhoto(): void {
    // Allow user to upload and set their profile photo using their camera or photo gallery.
    if (this.network.online()) {
      this.actionSheetCtrl.create({
        title: this.translate.get('auth.profile.photo.title'),
        buttons: [
          {
            text: this.translate.get('auth.profile.photo.take'),
            role: 'destructive',
            handler: () => {
              this.storage.upload(this.userId, this.storage.profilePhoto, this.camera.PictureSourceType.CAMERA).then((url: string) => {
                this.storage.delete(this.userId, this.photo);
                this.photo = url;
              }).catch(() => { });
            }
          },
          {
            text: this.translate.get('auth.profile.photo.gallery'),
            handler: () => {
              this.storage.upload(this.userId, this.storage.profilePhoto, this.camera.PictureSourceType.PHOTOLIBRARY).then((url: string) => {
                this.storage.delete(this.userId, this.photo);
                this.photo = url;
              }).catch(() => { });
            }
          },
          {
            text: this.translate.get('auth.profile.photo.cancel'),
            role: 'cancel',
            handler: () => { }
          }
        ]
      }).present();
    }
  }

}
