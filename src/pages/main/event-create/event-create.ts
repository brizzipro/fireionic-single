import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  Loading,
  LoadingController,
  AlertController,
  Alert,
} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirestoreProvider } from '../../../providers/firestore/firestore';

import { Event } from '../../../models';

@IonicPage({
  name: 'page-event-create',
  segment: 'event-create'
})

@Component({
  selector: 'page-event-create',
  templateUrl: 'event-create.html',
})
export class EventCreatePage {
  public createEventForm: FormGroup; // This is the form we're creating.
  public event: Event;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public firestoreProvider: FirestoreProvider,
    formBuilder: FormBuilder
  ) {
    this.event = this.navParams.get('event');

    if (this.event === undefined) {
      this.event = new Event('', '', '', '', new Date().toISOString().split('T')[0]);
    }

    this.createEventForm = formBuilder.group({
      eventName: [this.event.eventName, Validators.required],
      eventDescription: [this.event.eventDescription, Validators.required],
      eventLocation: [this.event.eventLocation, Validators.required],
      eventDate: [this.event.eventDate, Validators.required],
    });
  }

  ionViewDidLoad() {
  }

  manageEvent(): void {
    const loading: Loading = this.loadingCtrl.create();
    loading.present();

    const eventName = this.createEventForm.value.eventName;
    const eventDescription = this.createEventForm.value.eventDescription;
    const eventLocation = this.createEventForm.value.eventLocation;
    const eventDate = this.createEventForm.value.eventDate;

    if (this.event.id === '') {
      // console.log('create');
      this.firestoreProvider
        .createEvent(eventName, eventDescription, eventLocation, eventDate)
        .then(
          () => {
            loading.dismiss().then(() => {
              this.navCtrl.pop();
            });
          },
          error => {
            loading.dismiss().then(() => {
              const alert: Alert = this.alertCtrl.create({
                message: error.message,
                buttons: [{ text: 'Ok', role: 'cancel' }],
              });
              alert.present();
            });
          }
        );

    } else {
      // console.log('update');
      this.firestoreProvider
        .updateEvent(this.event.id, eventName, eventDescription, eventLocation, eventDate)
        .then(
          () => {
            loading.dismiss().then(() => {
              this.navCtrl.push('page-event-list');
            });
          },
          error => {
            loading.dismiss().then(() => {
              const alert: Alert = this.alertCtrl.create({
                message: error.message,
                buttons: [{ text: 'Ok', role: 'cancel' }],
              });
              alert.present();
            });
          }
        );

    }
  }

  updateEvent(): void {
    const loading: Loading = this.loadingCtrl.create();
    loading.present();

    const eventName = this.createEventForm.value.eventName;
    const eventDescription = this.createEventForm.value.eventDescription;
    const eventLocation = this.createEventForm.value.eventLocation;
    const eventDate = this.createEventForm.value.eventDate;

    this.firestoreProvider
      .createEvent(eventName, eventDescription, eventLocation, eventDate)
      .then(
        () => {
          loading.dismiss().then(() => {
            this.navCtrl.pop();
          });
        },
        error => {
          loading.dismiss().then(() => {
            const alert: Alert = this.alertCtrl.create({
              message: error.message,
              buttons: [{ text: 'Ok', role: 'cancel' }],
            });
            alert.present();
          });
        }
      );
  }

  showDate(evt) {
    console.log(evt);
    console.log(typeof this.createEventForm.get('eventDate').value);
  }

}
