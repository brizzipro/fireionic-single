import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  Loading,
  LoadingController,
  AlertController,
  Alert,
} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirestoreProvider } from '../../../providers/firestore/firestore';

import { Song } from '../../../models';

@IonicPage({
  name: 'page-song-create',
  segment: 'song-create'
})

@Component({
  selector: 'page-song-create',
  templateUrl: 'song-create.html',
})
export class SongCreatePage {
  public createSongForm: FormGroup; // This is the form we're creating.
  public song: Song;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public firestoreProvider: FirestoreProvider,
    formBuilder: FormBuilder
  ) {
    this.song = this.navParams.get('song');

    if (this.song === undefined) {
      this.song = new Song('', '', '', '', '');
    }

    this.createSongForm = formBuilder.group({
      albumName: [this.song.albumName, Validators.required],
      artistName: [this.song.artistName, Validators.required],
      songDescription: [this.song.songDescription, Validators.required],
      songName: [this.song.songName, Validators.required],
    });
  }

  ionViewDidLoad() {
  }

  manageSong(): void {
    const loading: Loading = this.loadingCtrl.create();
    loading.present();

    const albumName = this.createSongForm.value.albumName;
    const artistName = this.createSongForm.value.artistName;
    const songDescription = this.createSongForm.value.songDescription;
    const songName = this.createSongForm.value.songName;

    // console.log(this.song);

    if (this.song.id === '') {
      console.log('create');
      this.firestoreProvider
        .createSong(albumName, artistName, songDescription, songName)
        .then(
          () => {
            loading.dismiss().then(() => {
              this.navCtrl.pop();
            });
          },
          error => {
            loading.dismiss().then(() => {
              const alert: Alert = this.alertCtrl.create({
                message: error.message,
                buttons: [{ text: 'Ok', role: 'cancel' }],
              });
              alert.present();
            });
          }
        );

    } else {
      // console.log('update');
      this.firestoreProvider
        .updateSong(this.song.id, albumName, artistName, songDescription, songName)
        .then(
          () => {
            loading.dismiss().then(() => {
              this.navCtrl.push('page-song-list');
            });
          },
          error => {
            loading.dismiss().then(() => {
              const alert: Alert = this.alertCtrl.create({
                message: error.message,
                buttons: [{ text: 'Ok', role: 'cancel' }],
              });
              alert.present();
            });
          }
        );

    }
  }

  updateSong(): void {
    const loading: Loading = this.loadingCtrl.create();
    loading.present();

    const albumName = this.createSongForm.value.albumName;
    const artistName = this.createSongForm.value.artistName;
    const songDescription = this.createSongForm.value.songDescription;
    const songName = this.createSongForm.value.songName;

    this.firestoreProvider
      .createSong(albumName, artistName, songDescription, songName)
      .then(
        () => {
          loading.dismiss().then(() => {
            this.navCtrl.pop();
          });
        },
        error => {
          loading.dismiss().then(() => {
            const alert: Alert = this.alertCtrl.create({
              message: error.message,
              buttons: [{ text: 'Ok', role: 'cancel' }],
            });
            alert.present();
          });
        }
      );
  }

}
