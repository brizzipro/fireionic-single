import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SongCreatePage } from './song-create';

@NgModule({
  declarations: [
    SongCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(SongCreatePage),
    TranslateModule.forChild()
  ],
})
export class SongCreatePageModule {}
