import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { FirestoreProvider } from '../../../providers/firestore/firestore';

import { Event } from '../../../models';

@IonicPage({
  name: 'page-event-detail',
  segment: 'event-detail'
})

@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html',
})
export class EventDetailPage {
  public event: Event;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public firestore: FirestoreProvider) {
    this.event = this.navParams.get('event');
  }

  ionViewDidLoad() {
  }

  deleteEvent(eventId: string, eventName: string): void {
    const alert: Alert = this.alertCtrl.create({
      message: `Are you sure you want to delete ${eventName} from your list?`,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Clicked Cancel');
          },
        },
        {
          text: 'OK',
          handler: () => {
            this.firestore.deleteEvent(eventId).then(() => {
              this.navCtrl.pop();
            });
          },
        },
      ],
    });
    alert.present();
  }

  editEvent(event: Event): void {
    this.navCtrl.push('page-event-create', { event: event });
  }

}
