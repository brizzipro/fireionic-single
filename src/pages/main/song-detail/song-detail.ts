import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { FirestoreProvider } from '../../../providers/firestore/firestore';

import { Song } from '../../../models';

@IonicPage({
  name: 'page-song-detail',
  segment: 'song-detail'
})

@Component({
  selector: 'page-song-detail',
  templateUrl: 'song-detail.html',
})
export class SongDetailPage {
  public song: Song;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public firestore: FirestoreProvider) {
    this.song = this.navParams.get('user');
  }

  ionViewDidLoad() {
  }

  deleteSong(songId: string, songName: string): void {
    const alert: Alert = this.alertCtrl.create({
      message: `Are you sure you want to delete ${songName} from your list?`,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Clicked Cancel');
          },
        },
        {
          text: 'OK',
          handler: () => {
            this.firestore.deleteSong(songId).then(() => {
              this.navCtrl.pop();
            });
          },
        },
      ],
    });
    alert.present();
  }

  editSong(song: Song): void {
    this.navCtrl.push('page-song-create', { song: song });
  }

}
