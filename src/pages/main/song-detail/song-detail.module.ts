import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SongDetailPage } from './song-detail';

@NgModule({
  declarations: [
    SongDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SongDetailPage),
    TranslateModule.forChild()
  ],
})
export class SongDetailPageModule {}
