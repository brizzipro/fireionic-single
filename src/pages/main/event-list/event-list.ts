import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { FirestoreProvider } from '../../../providers/firestore/firestore';

import { Event } from '../../../models';

@IonicPage({
  name: 'page-event-list',
  segment: 'event-list'
})

@Component({
  selector: 'page-event-list',
  templateUrl: 'event-list.html',
})

export class EventListPage {
  public eventList: Observable<Event[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private firestore: FirestoreProvider) {
  }

  ionViewDidLoad() {
    this.eventList = this.firestore.getEventList().valueChanges();
  }

  goToDetailPage(event: Event): void {
    this.navCtrl.push('page-event-detail', { event: event });
  }

}
