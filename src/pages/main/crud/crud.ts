import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Pages } from '../../../interfaces/pages'

@IonicPage({
  name: 'page-crud',
  segment: 'crud'
})

@Component({
  selector: 'page-crud',
  templateUrl: 'crud.html',
})

export class CrudPage {
  pages: Array<Pages>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.pages = [
      { title: 'Song List', component: 'page-song-list', icon: 'musical-notes' },
      { title: 'Event List', component: 'page-event-list', icon: 'calendar' }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrudPage');
  }

  openPage(page) {
    this.navCtrl.push(page.component);
  }

}
