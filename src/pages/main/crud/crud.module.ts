import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { CrudPage } from './crud';

@NgModule({
  declarations: [
    CrudPage,
  ],
  imports: [
    IonicPageModule.forChild(CrudPage),
    TranslateModule.forChild()
  ],
})
export class CrudPageModule {}
