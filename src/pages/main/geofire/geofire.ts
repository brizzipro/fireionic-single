import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { FirestoreProvider } from '../../../providers';

import { GeoMap } from '../../../models';

import { Geolocation } from '@ionic-native/geolocation';

@IonicPage({
  name: 'page-geofire',
  segment: 'geofire'
})

@Component({
  selector: 'page-geofire',
  templateUrl: 'geofire.html',
})
export class GeofirePage {

  lat: number;
  lng: number;
  markers: any;
  private subscription: Subscription;

  constructor(private firestore: FirestoreProvider, private geolocation: Geolocation) {
    this.getUserLocation();
  }

  ionViewDidLoad() {
    // Get all Locations registered on Firestore.
    this.subscription = this.firestore.getAllLocations().valueChanges().subscribe((markers: GeoMap[]) => {
      this.markers = markers;
    });
   }


  private getUserLocation() {
    /// locate the user
    this.geolocation.getCurrentPosition().then((res) => {
      // console.log(res);
      this.lat = 41.3828939;
      this.lng = 2.1774322;
      this.firestore.setLocation([this.lat, this.lng]);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    // if (navigator.geolocation) {
    //   navigator.geolocation.getCurrentPosition(position => {
    //     this.lat = position.coords.latitude;
    //     this.lng = position.coords.longitude;
    //     this.firestore.setLocation([this.lat, this.lng]);
    //   });
    // }
  }
}
