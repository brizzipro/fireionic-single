import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { AgmCoreModule } from '@agm/core';
import { GeofirePage } from './geofire';

import { Environment } from '../../../environment/environment';

@NgModule({
  declarations: [
    GeofirePage,
  ],
  imports: [
    IonicPageModule.forChild(GeofirePage),
    TranslateModule.forChild(),
    AgmCoreModule.forRoot({
      apiKey: Environment.googleMapsKey
    })
  ],
})
export class GeofirePageModule { }
