import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SongListPage } from './song-list';

@NgModule({
  declarations: [
    SongListPage,
  ],
  imports: [
    IonicPageModule.forChild(SongListPage),
    TranslateModule.forChild()
  ],
})
export class SongListPageModule {}
