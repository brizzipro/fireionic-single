import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { FirestoreProvider } from '../../../providers/firestore/firestore';

import { Song } from '../../../models';

@IonicPage({
  name: 'page-song-list',
  segment: 'song-list'
})

@Component({
  selector: 'page-song-list',
  templateUrl: 'song-list.html',
})

export class SongListPage {
  public songList: Observable<Song[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private firestore: FirestoreProvider,) {
  }



}
