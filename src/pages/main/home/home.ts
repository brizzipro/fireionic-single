import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { AuthProvider, NotificationProvider } from '../../../providers';
import { Subscription } from 'rxjs/Subscription';

@IonicPage({
  name: 'page-home',
  segment: 'home',
  priority: 'high'
})

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  private subscription: Subscription;

  constructor(
    private navCtrl: NavController,
    public auth: AuthProvider,
    private notification: NotificationProvider,
    private menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    // this.menuCtrl.swipeEnable(true, 'authenticated');
    this.menuCtrl.enable(true);

    // if (this.auth.getUserData().notifications) {
    //   this.notification.init();
    // }
  }

  ionViewWillUnload() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
