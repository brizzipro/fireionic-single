export class GeoMap {
  public obj: {};
  constructor(
    public g: string,
    public l: Array<number>
  ) {
    this.obj = {
      g: this.g,
      l: this.l
    };
  }
}
