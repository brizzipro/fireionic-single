export class User {
  public obj: {};
  constructor(
    public userId: string,
    public email: string,
    public nombre: string,
    public apellido1: string,
    public photo: string,
    public username: string,
    public pushToken: string,
    public online: boolean
  ) {
    this.obj = {
      userId: userId,
      email: this.email,
      nombre: this.nombre,
      apellido1: this.apellido1,
      photo: this.photo,
      username: this.username,
      pushToken: this.pushToken,
      online: this.online
    };
  }
}
